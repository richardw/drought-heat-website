from django.conf.urls import include, url
from django.contrib import admin
from . import views
from atlas.views import NewsDetailView

admin.autodiscover()

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^news/(?P<pk>\d+)/$', NewsDetailView.as_view(template_name='atlas/news_detail.html'), name='news-detail'),
    url(r'^about/contact/$', views.TemplateView.as_view(template_name='atlas/contact.html'), name='contact'), # contact
    url(r'^about/imprint/$', views.TemplateView.as_view(template_name='atlas/imprint.html'), name='imprint'), # imprint
    url(r'^work-packages/work-package_1$', views.TemplateView.as_view(template_name='atlas/work-package_1.html'), name='work-package_1'), # work package 1
    url(r'^work-packages/work-package_2$', views.TemplateView.as_view(template_name='atlas/work-package_2.html'), name='work-package_2'), # work package 2
    url(r'^work-packages/work-package_3$', views.TemplateView.as_view(template_name='atlas/work-package_3.html'), name='work-package_3'), # work package 3
    url(r'^team/contributors/$', views.TemplateView.as_view(template_name='atlas/contributors.html'), name='contributors'), # contributors
    url(r'^team/collaborators/$', views.TemplateView.as_view(template_name='atlas/collaborators.html'), name='collaborators'), # collaborators
    url(r'^publications/$', views.TemplateView.as_view(template_name='atlas/publications.html'), name='publications'), # publications list
    url(r'^atlas/$', views.TemplateView.as_view(template_name='atlas/plot.html'), name='diagnostic_atlas'), # diagnostic atlas
    url(r'^atlas_iframe/$', views.TemplateView.as_view(template_name='atlas/plot_iframe.html'), name='diagnostic_atlas_iframe') # diagnostic atlas iframe (to be emedded into ETH land-climate atlas)
]
