from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.views.generic.detail import DetailView
from atlas.models import Post, Author

def index(request):
    all_posts = Post.objects.all().order_by('-date')
    template_data = {'posts' : all_posts}
    return render_to_response('atlas/index.html', template_data)

class NewsDetailView(DetailView):
    model = Post
    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        return context

class TemplateView(generic.ListView):
    template_name = 'droughtheat/collaborators.html'
    def get_queryset(self):
        return 1
